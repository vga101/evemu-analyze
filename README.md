EVEMUL TOUCH POINT ANALYZER
===========================

Quick analysis hack to find bad touch points. 
VG, 2018

Additional information:
  - https://who-t.blogspot.com/2016/09/understanding-evdev.html
  - https://www.kernel.org/doc/html/latest/input/multi-touch-protocol.html

# Usage

Store raw data and print analyzed format
```
$ sudo evemu-record | tee recording.evemu | ./touch-analyze.py
```

Potentially set PYTHONUNBUFFERED=1 for unbuffered output, e.g. pipelining
