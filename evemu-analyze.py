#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
===========================
EVEMUL TOUCH POINT ANALYZER
===========================

Quick analysis hack to find bad touch points
Volker Gaibler, 2018

https://who-t.blogspot.com/2016/09/understanding-evdev.html
https://www.kernel.org/doc/html/latest/input/multi-touch-protocol.html

Usage: stores raw data and prints analyzed format
$ sudo evemu-record | tee recording.evemu | ./touch-analyze.py

potentially set PYTHONUNBUFFERED=1 for unbuffered output, e.g. pipelining
"""

import logging
import numpy as np
import sys


max_touchpoints = -1
current_slot = -1
time = 0.
last_time = 0.
counter = 0
syn_report_counter = 0
marker_dt = 1.0   # time interval that is marked with empty line

# input:
infile = sys.stdin


print("==== Analyzing EVEMUL output ====\n")

### read header
#

while True:
    line = infile.readline()
    if not line.startswith("# "):
        break

    if "(ABS_MT_SLOT)" in line:
        # current value in next line
        line = infile.readline()
        current_slot = int(line.split()[2])
        line = infile.readline()
        slot_min = int(line.split()[2])
        line = infile.readline()
        slot_max = int(line.split()[2])
        max_touchpoints = slot_max - slot_min + 1

        if slot_min != 0:
            raise NotImplementedError
        break

if current_slot == -1:
    print("Error: value for ABS_MT_SLOT not found.")
    sys.exit(1)

if max_touchpoints == -1:
    print("Error: number of slots unknown.")
    sys.exit(1)

# evolving touch point state
touch = np.full((max_touchpoints, 2), -1, dtype=int)

def print_toucharray():
    print("{:5} {:11.6f}  ".format(counter, time), end="")
    for e in touch:
        print("[{:5} {:5}]".format(e[0], e[1]), end=" ")
    print()



### read data, only events are interesting
#

while True:
    line = infile.readline()

    # end of file
    if line == "":
        print("\nEND OF FILE")
        print("{:6d} touch events in {:8.3f} seconds: {:8.3f} touch events per second".format(
            counter, time, counter/time))
        print("{:6d} SYN_REPORT in   {:8.3f} seconds: {:8.3f} SYN_REPORT per second".format(
            syn_report_counter, time, syn_report_counter/time))
        sys.exit(0)

    # non-events
    if not line.startswith("E: "):
        continue

    # events only:
    #
    last_time = time
    time = float(line.split()[1])
    if time - last_time > marker_dt:
        print()  # empty line separates large time interval in between

    # all events go into counter
    counter += 1

    # slot change
    if "EV_ABS / ABS_MT_SLOT" in line:
        current_slot = int(line.split()[9])
        continue

    # x update
    elif "EV_ABS / ABS_MT_POSITION_X" in line:
        x = int(line.split()[9])
        touch[current_slot][0] = x

    # y update
    elif "EV_ABS / ABS_MT_POSITION_Y" in line:
        y = int(line.split()[9])
        touch[current_slot][1] = y

    elif "EV_ABS / ABS_MT_TRACKING_ID" in line:
        tid = int(line.split()[9])
        if tid == -1:
            # touch ended, set dummy value
            touch[current_slot][0] = -1
            touch[current_slot][1] = -1
            # print("touch up on slot {}".format(current_slot))

    #elif "EV_KEY / BTN_TOUCH" in line:
    #    btn_touch = int(line.split()[9])
    #    if btn_touch == 0:
    #        # touch ended, set dummy value
    # --> not using this because there is only one even if two slots have touch end, use tracking id == -1 for this


    # SYN_REPORT: all preceeding events belong together, no order (except of course slot change)
    # so only print once all message of one batch are processed
    # otherwise e.g. x updated, but not yet y!
    elif "------------ SYN_REPORT (0) ----------" in line:
        syn_report_counter += 1
        print_toucharray()

